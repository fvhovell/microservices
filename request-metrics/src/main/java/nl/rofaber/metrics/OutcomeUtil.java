package nl.rofaber.metrics;

@SuppressWarnings("PMD.TooManyStaticImports")
public class OutcomeUtil {

  public static String outcome(Integer status) {
    if (status == null) {
      return RequestMetricsService.SUCCESS;
    }
    if (status < 200) {
      return RequestMetricsService.INFORMATIONAL;
    } else if (status < 300) {
      return RequestMetricsService.SUCCESS;
    } else if (status < 400) {
      return RequestMetricsService.REDIRECTION;
    } else {
      return status < 500 ? RequestMetricsService.CLIENT_ERROR : RequestMetricsService.SERVER_ERROR;
    }
  }
}
