package nl.rofaber.metrics.config;

import nl.rofaber.metrics.epos.EposMetricsAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class EposMetricsConfiguration {

  @Bean
  public EposMetricsAspect eposMetricsAspect() {
    return new EposMetricsAspect();
  }
}
