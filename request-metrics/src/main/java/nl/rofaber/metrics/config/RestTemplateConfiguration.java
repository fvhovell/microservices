package nl.rofaber.metrics.config;

import nl.rofaber.metrics.http.outgoing.RestTemplateMetricsInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class RestTemplateConfiguration {
  @Autowired
  private RestTemplate restTemplate;

  @Bean
  public RestTemplateMetricsInterceptor restTemplateMetricsInterceptor() {
    List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
    if (CollectionUtils.isEmpty(interceptors)) {
      interceptors = new ArrayList<>();
    }
    RestTemplateMetricsInterceptor interceptor = new RestTemplateMetricsInterceptor();
    interceptors.add(interceptor);
    restTemplate.setInterceptors(interceptors);
    return interceptor;
  }
}
