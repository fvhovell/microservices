package nl.rofaber.metrics.config;

import nl.rofaber.metrics.database.JdbcTemplateMetricsAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class JdbcTemplateMetricsConfiguration {

  @Bean
  public JdbcTemplateMetricsAspect jdbcTemplateMetricsAspect() {
    return new JdbcTemplateMetricsAspect();
  }
}
