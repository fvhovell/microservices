package nl.rofaber.metrics.config;

import feign.Feign;
import lombok.extern.slf4j.Slf4j;
import nl.rofaber.metrics.RequestMetricsService;
import nl.rofaber.metrics.http.outgoing.FeignInvocationHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
@Slf4j
public class FeignMetricsConfiguration {

  @Autowired
  private RequestMetricsService requestMetricsService;

  @Bean
  public Feign.Builder feignBuilder() {
    return Feign.builder().invocationHandlerFactory((target, map) -> new FeignInvocationHandler(requestMetricsService, target, map));
  }
}
