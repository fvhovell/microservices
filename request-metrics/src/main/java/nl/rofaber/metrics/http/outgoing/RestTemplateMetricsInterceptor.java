package nl.rofaber.metrics.http.outgoing;

import nl.rofaber.metrics.OutcomeUtil;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class RestTemplateMetricsInterceptor implements ClientHttpRequestInterceptor {

  @Autowired
  private RequestMetricsService requestMetricsService;

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

    List<String> serviceIdHeaders = request.getHeaders().get(RequestMetricsService.TO_SERVICE_HEADER);
    String serviceId = serviceIdHeaders != null && serviceIdHeaders.size() > 0 ? serviceIdHeaders.get(0) : "unknown";
    String url = request.getURI().toString();
    try {
      ClientHttpResponse response = execution.execute(request, body);
      String outcome = OutcomeUtil.outcome(response.getRawStatusCode());
      requestMetricsService.outgoingRequest(serviceId, url, outcome);
      return response;
    } catch (IOException e) {
      requestMetricsService.outgoingRequest(serviceId, url, RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }
}
