package nl.rofaber.metrics.annotations;

import nl.rofaber.metrics.config.EposMetricsConfiguration;
import nl.rofaber.metrics.config.RequestMetricsConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({
    RequestMetricsConfiguration.class,
    EposMetricsConfiguration.class,
})
public @interface EnableEposMetrics {
}
