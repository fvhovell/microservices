package nl.rofaber.metrics.annotations;

import nl.rofaber.metrics.config.JdbcTemplateMetricsConfiguration;
import nl.rofaber.metrics.config.RequestMetricsConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({
    RequestMetricsConfiguration.class,
    JdbcTemplateMetricsConfiguration.class,
})
public @interface EnableJdbcTemplateMetrics {
}
