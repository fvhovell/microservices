package nl.rofaber.metrics.database;

import nl.rofaber.metrics.RequestMetricsService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class JdbcTemplateMetricsAspect {

  @Autowired
  private RequestMetricsService requestMetricsService;

//  @Around("execution(public * org.springframework.jdbc.core.JdbcOperations+.*(..))")
//  public Object jdbcOperationAspect(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//    return handle(proceedingJoinPoint);
//  }

  @Around("execution(public * org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations+.*(..))")
  public Object namedParamJdbcOperationAspect(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    return handle(proceedingJoinPoint);
  }

  private Object handle(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

    String className = methodSignature.getDeclaringType().getSimpleName();
    String methodName = methodSignature.getName();

    String databaseAction = String.format("%s->%s", className, methodName);

    try {
      Object result = proceedingJoinPoint.proceed();
      requestMetricsService.outgoingRequest("database", databaseAction, RequestMetricsService.SUCCESS);
      return result;
    } catch (Throwable t) {
      requestMetricsService.outgoingRequest("database", databaseAction, RequestMetricsService.SERVER_ERROR);
      throw t;
    }
  }
}
