package nl.rofaber.metrics;

import io.micrometer.core.instrument.Tags;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Metric {

  private String name;
  private Tags tags;
}
