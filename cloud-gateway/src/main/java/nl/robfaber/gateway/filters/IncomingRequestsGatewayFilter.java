package nl.robfaber.gateway.filters;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.rofaber.metrics.OutcomeUtil;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@RequiredArgsConstructor
public class IncomingRequestsGatewayFilter implements GlobalFilter {
  private final RequestMetricsService requestMetricsService;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    val url = exchange.getRequest().getPath().toString();
    log.info("Incoming request {}", url);
    return chain.filter(exchange)
        .then(Mono.fromRunnable(() -> {
          val outcome = OutcomeUtil.outcome(exchange.getResponse().getRawStatusCode());
          log.info("Incoming request {} -> {}", url, outcome);
          requestMetricsService.incomingRequest("INTERNET", url, outcome);
        }));
  }
}