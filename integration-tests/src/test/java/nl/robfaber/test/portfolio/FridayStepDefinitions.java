package nl.robfaber.test.portfolio;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FridayStepDefinitions {

  private String day;
  private String result;

  @Given("today is {string}")
  public void todayIs(String day) {
    this.day = day;
  }

  @When("I ask whether it's {string} yet")
  public void iAskWhetherItSFridayYet(String test) {
    result = (day.equals(test) ? "Yeah" : "Nope");
  }

  @Then("I should be told {string}")
  public void iShouldBeTold(String expected) {
    assertEquals(result, expected);
  }
}
