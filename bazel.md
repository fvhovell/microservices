bazel build $(bazel query //...)

bazel test $(bazel query //...)

bazel test --test_output=all //admin-server:AllTests

bazel build //admin-server:admin-server_deploy.jar
bazel build //service-registry:service-registry_deploy.jar