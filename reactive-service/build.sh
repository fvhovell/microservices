mvn clean install -DskipTests

pack build robfaber/reactive-service --path target/reactive-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

cd .. && docker-compose up -d
cd reactive-service
