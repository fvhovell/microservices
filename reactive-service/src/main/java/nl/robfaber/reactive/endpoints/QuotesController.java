package nl.robfaber.reactive.endpoints;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.quote.model.Quote;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QuotesController {
  private final WebClient webClient;

  @GetMapping("/quotes")
  public Flux<Quote> quotes() {
    log.info("Getting quotes from quotes-service");
    return webClient
        .get().uri("http://quote-service/quotes")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(Quote.class);
  }
}
