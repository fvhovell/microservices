package nl.robfaber.reactive.config;

import lombok.RequiredArgsConstructor;
import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableDiscoveryClient
@EnableRequestMetrics
@RequiredArgsConstructor
public class ReactiveConfig {
  private final ReactorLoadBalancerExchangeFilterFunction lbFunction;
  private final ReactiveOutgoingMetricsExchangeFilter metricsFilter;

  @Bean
  public WebClient webClient() {
    return WebClient.builder()
        .filter(metricsFilter)
        .filter(lbFunction)
        .build();
  }
}

