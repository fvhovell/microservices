mvn clean install -DskipTests

pack build robfaber/quote-service --path target/quote-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

cd .. && docker-compose up -d
cd quote-service
