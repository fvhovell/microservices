package nl.robfaber.quote.service.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.quote.model.Quote;
import nl.robfaber.quote.service.port.QuotePort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/quotes")
public class QuoteEndpoint {
  private final QuotePort quotePort;

  @GetMapping
  public List<Quote> quotes() {
    return quotePort.getQuotes();
  }

  @GetMapping("/{symbol}")
  public Quote quote(@PathVariable("symbol") final String symbol) {
    return quotePort.getQuote(symbol);
  }
}
