mvn clean install -DskipTests

pack build robfaber/experience-service --path target/experience-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

cd .. && docker-compose up -d
cd experience-service
