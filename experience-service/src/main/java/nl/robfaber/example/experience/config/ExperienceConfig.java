package nl.robfaber.example.experience.config;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import nl.rofaber.metrics.annotations.EnableFeignMetrics;
import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import nl.rofaber.metrics.annotations.EnableRestTemplateMetrics;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableScheduling
@EnableRequestMetrics
@EnableFeignMetrics
@EnableRestTemplateMetrics
public class ExperienceConfig {

  @Value("${spring.application.name}")
  private String applicationName;

  @Bean
  public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() throws UnknownHostException {
    List<Tag> tags = Arrays.asList(
        Tag.of("application", applicationName),
        Tag.of("host", InetAddress.getLocalHost().getHostName())
    );

    return registry -> registry.config().commonTags(tags);
  }

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
