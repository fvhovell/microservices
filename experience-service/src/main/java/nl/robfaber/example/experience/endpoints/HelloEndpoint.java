package nl.robfaber.example.experience.endpoints;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.example.experience.dto.HelloResponse;
import nl.robfaber.example.experience.dto.Instrument;
import nl.robfaber.example.experience.services.CapabilityClient;
import nl.robfaber.example.experience.services.QuoteServiceClient;
import nl.robfaber.quote.model.Quote;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
@Slf4j
@RequiredArgsConstructor
public class HelloEndpoint {

  private final CapabilityClient capabilityClient;
  private final QuoteServiceClient quoteServiceClient;

  @Value("${failures:false}")
  private boolean failures;

  @GetMapping("/hello")
  public ResponseEntity<HelloResponse> hello() {
    if (failures && new Random().nextInt(100) < 10) {
      log.info("Failing a request with 500");
      return ResponseEntity.status(500).build();
    }
    return ResponseEntity.ok(capabilityClient.hello());
  }

  @GetMapping("/hello-fail")
  public ResponseEntity<HelloResponse> helloFail() {
    return ResponseEntity.status(500).build();
  }

  @GetMapping("/instruments")
  public List<Instrument> instruments() {
    return capabilityClient.getInstruments();
  }

  @GetMapping("/quotes")
  public List<Quote> getQuotes() {
    return quoteServiceClient.getQuotes();
  }

  @GetMapping("/quotes/{symbol}")
  public Quote getQuote(@PathVariable("symbol") String symbol) {
    return quoteServiceClient.getQuote(symbol);
  }
}
