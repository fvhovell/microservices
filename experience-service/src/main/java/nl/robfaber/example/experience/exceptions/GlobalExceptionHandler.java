package nl.robfaber.example.experience.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
public class GlobalExceptionHandler {

  @ExceptionHandler({ RuntimeException.class })
  public ResponseEntity<?> handleException() {
     return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }
}
