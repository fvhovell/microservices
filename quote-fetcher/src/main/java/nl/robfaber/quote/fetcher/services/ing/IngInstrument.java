package nl.robfaber.quote.fetcher.services.ing;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class IngInstrument {
  private String uid;
  private String symbol;
  private String name;
  private String isin;
  private String exchange;
  private String currency;
  private ZonedDateTime datetime;
  private IngPrice currentPriceValue;
  private IngPrice closePriceValue;
  private IngPrice highPriceValue;
  private IngPrice lowPriceValue;
  private IngPrice openPriceValue;
}
