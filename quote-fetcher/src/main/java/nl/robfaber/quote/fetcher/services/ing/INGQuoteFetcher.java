package nl.robfaber.quote.fetcher.services.ing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.robfaber.quote.fetcher.config.QuoteFetcherProperties;
import nl.robfaber.quote.model.Quote;
import nl.robfaber.quote.model.Stock;
import nl.rofaber.metrics.RequestMetricsService;
import org.apache.commons.codec.binary.Hex;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Service
@RequiredArgsConstructor
@Slf4j
public class INGQuoteFetcher {
  private final QuoteFetcherProperties properties;
  private final RestTemplate restTemplate;
  private final KafkaTemplate<String, Object> kafkaTemplate;
  private final RequestMetricsService requestMetricsService;

  @Scheduled(fixedRate = 60_000L)
  public void fetch() {
    Arrays.asList(properties.getIngSymbols()).forEach(symbol -> {
      try {
        val stock = getStock(symbol);
        kafkaTemplate.send("stocks", symbol, stock);
        requestMetricsService.outgoingRequest("kafka", "stocks", RequestMetricsService.SUCCESS);
        kafkaTemplate.send("quotes", symbol, stock.getQuote());
        requestMetricsService.outgoingRequest("kafka", "quotes", RequestMetricsService.SUCCESS);
      } catch (Exception e) {
        log.warn("Failed to retrieve ING instrument {}", symbol, e);
      }
    });
  }

  private Stock getStock(String symbol) {
    try {
      val symbolJson = String.format("{\"symbol\": \"%s\"}", symbol);
      val hexed = Hex.encodeHexString(symbolJson.getBytes(StandardCharsets.UTF_8));
      val url = String.format("https://www.ing.nl/api/securities/web/markets/instruments/%s", hexed);
      val ingInstrument = restTemplate.getForObject(url, IngInstrument.class);
      requestMetricsService.outgoingRequest("ing-investments", "get-stocks", RequestMetricsService.SUCCESS);
      return map(ingInstrument);
    } catch (RuntimeException e) {
      requestMetricsService.outgoingRequest("ing-investments", "get-stocks", RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }

  private Stock map(IngInstrument instrument) {
    return Stock.builder()
        .currency(instrument.getCurrency())
        .stockExchange(instrument.getExchange())
        .name(instrument.getName())
        .quote(Quote.builder()
            .symbol(instrument.getSymbol())
            .open(instrument.getOpenPriceValue() == null ? null : instrument.getOpenPriceValue().getValue())
            .dayHigh(instrument.getHighPriceValue() == null ? null : instrument.getHighPriceValue().getValue())
            .dayLow(instrument.getLowPriceValue() == null ? null : instrument.getLowPriceValue().getValue())
            .price(instrument.getCurrentPriceValue() == null ? null : instrument.getCurrentPriceValue().getValue())
            .previousClose(instrument.getClosePriceValue() == null ? null : instrument.getClosePriceValue().getValue())
            .lastTrade(instrument.getDatetime())
            .build())
        .build();
  }
}
