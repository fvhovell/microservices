package nl.robfaber.quote.fetcher.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.quote.fetcher.services.yahoo.YahooQuoteFetcher;
import nl.robfaber.quote.model.Stock;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class QuoteEndpoint {
  private final YahooQuoteFetcher yahooQuoteFetcher;

  @GetMapping("/yahoo/quotes")
  public List<Stock> getQuotes() {
    return yahooQuoteFetcher.getStocks();
  }

  @GetMapping("/yahoo/quotes/{symbol}")
  public Stock getQuote(@PathVariable("symbol") String symbol) {
    return yahooQuoteFetcher.getStock(symbol);
  }
}
