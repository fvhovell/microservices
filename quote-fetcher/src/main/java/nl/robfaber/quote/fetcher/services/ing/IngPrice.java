package nl.robfaber.quote.fetcher.services.ing;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class IngPrice {
  private BigDecimal value;
  private String unit;
  private boolean percent;
}
