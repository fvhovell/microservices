package nl.robfaber.quote.fetcher.config;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.AbstractClientHttpResponse;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Configuration
@RequiredArgsConstructor
public class QuoteFetcherConfig {
  private static final byte[] JSON_PREFIX_BYTES = ")]}',\n".getBytes(StandardCharsets.UTF_8);

//  @Value("${spring.application.name}")
//  private String applicationName;
//
//  @Bean
//  public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
//    return registry -> registry.config()
//        .commonTags(Arrays.asList(Tag.of("application", applicationName)));
//  }

//  @Bean
//  public ProducerFactory<Object, Object> producerFactory() {
//    val pf = new DefaultKafkaProducerFactory<Object, Object>(Map.of(
//        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092",
//        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
//        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class
//    ));
//
////    //, Collections.singletonList(new ImmutableTag("customTag", "customTagValue"))
//    val metricsListener = new MicrometerProducerListener(meterRegistry);
//
//    pf.addListener(metricsListener);
//
//    return pf;
//  }
//
//  public KafkaTemplate<Object, Object> kafkaTemplate() {
//    return new KafkaTemplate<Object, Object>(producerFactory());
//  }


  @Bean
  public RestTemplate restTemplate() {
    val resTemplate = new RestTemplate();
    resTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
      @Override
      public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        final ClientHttpResponse response = execution.execute(request, body);
        return new AbstractClientHttpResponse() {
          @Override
          public int getRawStatusCode() throws IOException {
            return response.getRawStatusCode();
          }

          @Override
          public String getStatusText() throws IOException {
            return response.getStatusText();
          }

          @Override
          public void close() {
            response.close();
          }

          @Override
          public InputStream getBody() throws IOException {
            val input = new PushbackInputStream(response.getBody(), 6);
            if (input.available() < 6) {
              return input;
            }
            val jsonPrefix = new byte[6];
            input.read(jsonPrefix);
            if (!Arrays.equals(JSON_PREFIX_BYTES, jsonPrefix)) {
              input.unread(jsonPrefix);
            }
            return input;
          }

          @Override
          public HttpHeaders getHeaders() {
            return response.getHeaders();
          }
        };
      }
    });
    return resTemplate;
  }
}
