package nl.robfaber.portfolio.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Position {
  private Long id;
  private BigDecimal quantity;
  private Instrument instrument;
  private Money currentValue;
}
