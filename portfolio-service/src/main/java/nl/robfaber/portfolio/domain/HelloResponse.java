package nl.robfaber.portfolio.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HelloResponse {
  private String value;
  private String name;
}
