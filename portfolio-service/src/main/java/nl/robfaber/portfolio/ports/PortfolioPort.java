package nl.robfaber.portfolio.ports;

import nl.robfaber.portfolio.domain.Portfolio;

import java.util.List;

public interface PortfolioPort {
  List<Portfolio> getPortfolios();

  Portfolio getPortfolio(Long id);

  void savePortfolio(Portfolio portfolio);

  void deletePortfolio(Portfolio portfolio);
}
