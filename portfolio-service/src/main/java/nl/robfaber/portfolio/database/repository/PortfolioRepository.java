package nl.robfaber.portfolio.database.repository;

import nl.robfaber.portfolio.database.entities.PortfolioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortfolioRepository extends JpaRepository<PortfolioEntity, Long> {
}
