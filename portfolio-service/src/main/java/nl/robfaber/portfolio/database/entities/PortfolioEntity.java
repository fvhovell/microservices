package nl.robfaber.portfolio.database.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "PORTFOLIOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PortfolioEntity {
  @Id
  @GeneratedValue
  private long id;

  private String name;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "portfolio_id")
  private Set<PositionEntity> positions;
}
