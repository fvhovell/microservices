package nl.robfaber.portfolio.endpoints;

import lombok.RequiredArgsConstructor;
import lombok.val;
import nl.robfaber.portfolio.domain.Portfolio;
import nl.robfaber.portfolio.ports.PortfolioPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/portfolios")
@RequiredArgsConstructor
public class PortfolioEndpoint {

  private final PortfolioPort portfolioPort;

  @GetMapping
  public ResponseEntity<List<Portfolio>> getPortfolios() {
    return ResponseEntity.ok(portfolioPort.getPortfolios());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Portfolio> getPortfolio(@PathVariable("id") Long id) {
    return ResponseEntity.ok(portfolioPort.getPortfolio(id));
  }

  @PostMapping
  public ResponseEntity<Message> createPortfolio(@RequestBody Portfolio portfolio) {
    if (portfolio.getId() != null) {
      return ResponseEntity
          .badRequest()
          .body(Message.builder()
              .message("Portfolio already exists")
              .build());
    }
    portfolioPort.savePortfolio(portfolio);
    return ResponseEntity.ok(Message.builder().message("Portfolio created").build());
  }

  @PutMapping
  public ResponseEntity<Message> updatePortfolio(@RequestBody Portfolio portfolio) {
    portfolioPort.savePortfolio(portfolio);
    return ResponseEntity.ok(Message.builder().message("Portfolio updated").build());
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Message> deletePortfolio(@PathVariable("id") Long id) {
    val portfolio = portfolioPort.getPortfolio(id);
    if (portfolio == null) {
      return ResponseEntity
          .badRequest()
          .body(Message.builder().message("No such portfolio").build());
    }
    portfolioPort.deletePortfolio(portfolio);
    return ResponseEntity.ok(Message.builder().message("Portfolio deleted").build());
  }
}
