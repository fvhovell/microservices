package nl.robfaber.portfolio.endpoints;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Message {
  private String message;
}
