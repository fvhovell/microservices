package nl.robfaber.portfolio.endpoints;


import nl.robfaber.portfolio.domain.HelloResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class HelloEndpoint {

  @Value("${failures.enabled:false}")
  private boolean failures;
  @Value("${failures.percentage:10}")
  private int failurePercentage;


  @GetMapping("/hello")
  public HelloResponse hello() {
    if (failures && new Random().nextInt(100) < failurePercentage) {
      throw new RuntimeException("I fail because I can!");
    }
    return new HelloResponse("Hello", "key");
  }

  public HelloResponse defaultHello() {
    return new HelloResponse("Hello (fallback)", "key");
  }
}
