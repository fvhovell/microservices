package contracts.instruments

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/instruments'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""[
          {
            "id": 2,
            "name": "ING Groep"
          }
          ]""")
        headers {
            header(
                    'Content-Type': 'application/json',
            )
        }
    }
}