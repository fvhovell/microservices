#mvn clean install -DskipTests

pack build registry.robfaber.nl/robfaber/service-registry --path service-registry/target/service-registry-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/config-server --path config-server/target/config-server-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/admin-server --path admin-server/target/admin-server-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/cloud-gateway --path cloud-gateway/target/cloud-gateway-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/portfolio-service --path portfolio-service/target/portfolio-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/experience-service --path experience-service/target/experience-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/vizceral-server --path vizceral-server/target/vizceral-server-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/reactive-service --path reactive-service/target/reactive-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/quote-fetcher --path quote-fetcher/target/quote-fetcher-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base
pack build registry.robfaber.nl/robfaber/quote-service --path quote-service/target/quote-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

#docker tag
#docker-compose up -d
