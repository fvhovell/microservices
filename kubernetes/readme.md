### Deploy to kubernetes

```
    kubectl apply -f grafana.yaml
    kubectl apply -f zipkin.yaml
    kubectl apply -f nginx.yaml
    
```

To get external IP for nginx use;

```
    minikube service nginx
```

