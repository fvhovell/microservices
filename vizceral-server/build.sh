mvn clean install -DskipTests

pack build robfaber/vizceral-server --path target/vizceral-server-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

cd .. && docker-compose up -d
cd vizceral-server
