package nl.robfaber.vizceral.model.requests;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class MetricNames {
  List<String> names;

  public boolean contains(String metricName) {
    return names != null && names.contains(metricName);
  }
}
