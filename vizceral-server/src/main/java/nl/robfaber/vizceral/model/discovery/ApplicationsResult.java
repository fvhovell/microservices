package nl.robfaber.vizceral.model.discovery;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ApplicationsResult {
  private Applications applications;
}
