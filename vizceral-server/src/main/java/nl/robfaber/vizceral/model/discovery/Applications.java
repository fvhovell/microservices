package nl.robfaber.vizceral.model.discovery;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Applications {
  List<Application> application;
}
