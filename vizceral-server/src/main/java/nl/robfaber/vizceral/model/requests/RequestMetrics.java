package nl.robfaber.vizceral.model.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestMetrics {

  public static RequestMetrics empty = RequestMetrics.builder().build();

  private String name;
  private String description;
  private String baseUnit;
  private List<Measurement> measurements;
  private List<AvailableTags> availableTags;


  public List<AvailableTags> getAvailableTags() {
    if (availableTags == null) {
      return Collections.emptyList();
    }
    return availableTags;
  }

  public Measurement getCount() {
    if (measurements != null) {
      Optional<Measurement> count = getMeasurements().stream().filter(measurement -> measurement.getStatistic().equalsIgnoreCase("COUNT")).findFirst();
      if (count.isPresent()) {
        return count.get();
      }
      Optional<Measurement> value = getMeasurements().stream().filter(measurement -> measurement.getStatistic().equalsIgnoreCase("VALUE")).findFirst();
      if (value.isPresent()) {
        return value.get();
      }
    }
    return Measurement.builder()
        .value(0d)
        .statistic("COUNT")
        .build();
  }
}