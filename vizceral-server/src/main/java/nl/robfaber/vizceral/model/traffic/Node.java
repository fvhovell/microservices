package nl.robfaber.vizceral.model.traffic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Node {
  private String renderer;
  private String name;
  private String displayName;
  private List<Node> nodes;
  private List<Connection> connections;
  private Metadata metadata;
  @JsonProperty("class")
  private String clazz;
  private Long updated;
  @Builder.Default
  private Long maxVolume = 5000L;
}
