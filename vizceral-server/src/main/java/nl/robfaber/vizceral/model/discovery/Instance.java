package nl.robfaber.vizceral.model.discovery;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class Instance {
  private String instanceId;
  private String hostName;
  private String app;
  private String ipAddr;
  private String status;
  private String homePageUrl;
  private String statusPageUrl;
  private String healthCheckUrl;
  private Map<String, String> metadata;

  public String getDataCenter() {
    if (metadata == null) {
      return "MyOwn";
    }
    String dataCenter = metadata.get("datacenter");
    return (dataCenter == null) ? "MyOwn" : dataCenter;
  }
}
