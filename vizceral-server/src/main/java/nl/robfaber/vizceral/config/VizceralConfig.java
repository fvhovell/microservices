package nl.robfaber.vizceral.config;

import nl.rofaber.metrics.annotations.EnableRestTemplateMetrics;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableDiscoveryClient
@EnableRestTemplateMetrics
@EnableScheduling
public class VizceralConfig {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public WebMvcConfigurer forwardToIndex() {
    return new WebMvcConfigurer() {
      @Override
      public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/vizceral").setViewName("forward:/vizceral/index.html");
        registry.addViewController("/vizceral/").setViewName("forward:/vizceral/index.html");
      }
    };
  }
}
