package nl.robfaber.vizceral.services;

import lombok.RequiredArgsConstructor;
import nl.robfaber.vizceral.clients.MultiDiscoveryClient;
import nl.robfaber.vizceral.clients.RequestMetricsClient;
import nl.robfaber.vizceral.config.VizceralProperties;
import nl.robfaber.vizceral.model.discovery.Instance;
import nl.robfaber.vizceral.model.traffic.Connection;
import nl.robfaber.vizceral.model.traffic.Metrics;
import nl.robfaber.vizceral.model.traffic.Node;
import nl.robfaber.vizceral.model.traffic.Traffic;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static nl.rofaber.metrics.RequestMetricsService.INTERNET;

@Service
@RequiredArgsConstructor
public class TrafficService {
  public static final Traffic FAKE_TRAFFIC = Traffic.builder()
      .renderer("global")
      .name("edge")
      .serverUpdateTime(System.currentTimeMillis())
      .nodes(Arrays.asList(
          Node.builder()
              .renderer("region")
              .name(INTERNET)
              .clazz("normal")
              .build(),
          Node.builder()
              .renderer("region")
              .name("DCR")
              .clazz("normal")
              .maxVolume(5000L)
              .updated(System.currentTimeMillis())
              .nodes(Arrays.asList(
                  Node.builder()
                      .name(INTERNET)
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("api-gateway")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("web-xp-service")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("mobile-xp-service")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("portfolio-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("markets-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("order-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("watchlist-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("news-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build()
              ))
              .connections(Arrays.asList(
                  Connection.builder()
                      .source(INTERNET)
                      .target("api-gateway")
                      .metrics(Metrics.builder().normal(190.0).danger(10.0).build())
                      .build(),
                  Connection.builder()
                      .source("api-gateway")
                      .target("web-xp-service")
                      .metrics(Metrics.builder().normal(90.0).danger(10.0).build())
                      .build(),
                  Connection.builder()
                      .source("api-gateway")
                      .target("mobile-xp-service")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("portfolio-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("markets-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("order-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("watchlist-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("news-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("portfolio-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("markets-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("order-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("watchlist-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("news-api")
                      .metrics(Metrics.builder().normal(20.0).danger(80.0).build())
                      .build()

              ))
              .build(),
          Node.builder()
              .renderer("region")
              .name("WPR")
              .clazz("normal")
              .maxVolume(2000L)
              .updated(System.currentTimeMillis())
              .nodes(Arrays.asList(
                  Node.builder()
                      .name(INTERNET)
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("api-gateway")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("web-xp-service")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("mobile-xp-service")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("portfolio-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("markets-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("order-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("watchlist-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build(),
                  Node.builder()
                      .name("news-api")
                      .renderer("focusedChild")
                      .clazz("normal")
                      .build()
              ))
              .connections(Arrays.asList(
                  Connection.builder()
                      .source(INTERNET)
                      .target("api-gateway")
                      .metrics(Metrics.builder().normal(190.0).danger(10.0).build())
                      .build(),
                  Connection.builder()
                      .source("api-gateway")
                      .target("web-xp-service")
                      .metrics(Metrics.builder().normal(90.0).danger(10.0).build())
                      .build(),
                  Connection.builder()
                      .source("api-gateway")
                      .target("mobile-xp-service")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("portfolio-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("markets-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("order-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("watchlist-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("mobile-xp-service")
                      .target("news-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("portfolio-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("markets-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("order-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("watchlist-api")
                      .metrics(Metrics.builder().normal(100.0).danger(0.0).build())
                      .build(),
                  Connection.builder()
                      .source("web-xp-service")
                      .target("news-api")
                      .metrics(Metrics.builder().normal(20.0).danger(80.0).build())
                      .build()

              ))
              .build()
      ))
      .connections(Arrays.asList(
          Connection.builder()
              .source(INTERNET)
              .target("DCR")
              .metrics(Metrics.builder().normal(190.0).danger(10.0).build())
              .build(),
          Connection.builder()
              .source(INTERNET)
              .target("WPR")
              .metrics(Metrics.builder().normal(190.0).danger(100.0).build())
              .build()
      ))
      .build();
  private final VizceralProperties properties;
  private final MultiDiscoveryClient discoveryClient;
  private final RequestMetricsClient requestMetricsClient;
  private Traffic traffic;

  public Traffic getTraffic() {
    if (traffic == null) {
      return Traffic.builder()
          .renderer("global")
          .name("edge")
          .build();
    }
    return traffic;
//    return FAKE_TRAFFIC;
  }

  @Scheduled(fixedRate = 10_000L)
  public void updateTraffic() {
    //Refresh the data in the discovery client
    discoveryClient.refresh();

    //We first determine the different datacenters from the discovery client
    Set<String> dataCenters = discoveryClient.getDataCenters();

    //Create region nodes for every datacenter (including all the detailed traffic within that datacenter)
    //If there is no traffic we filter out the node because there is nothing to see
    List<Node> regionNodes = createRegionNodes(dataCenters).stream()
        .filter(node -> node.getName().equals(INTERNET) || !node.getConnections().isEmpty())
        .collect(Collectors.toList());

    //By introspecting the traffic in the different datacenters we can aggregrate it to an overview between the regions
    List<Connection> regionConnections = getRegionConnections(regionNodes);

    //Build the traffic output
    Traffic newTraffic = Traffic.builder()
        .renderer("global")
        .name("edge")
        .serverUpdateTime(System.currentTimeMillis())
        .nodes(regionNodes)
        .connections(regionConnections)
        .build();

    //Save the newTraffic as currentTraffic data
    traffic = newTraffic;
  }

  private List<Node> createRegionNodes(Set<String> dataCenters) {

    List<Node> nodes = new ArrayList<>();
    //We assume we always have a INTERNET connection (what is the point if we don't)
    nodes.add(Node.builder()
        .name(INTERNET)
        .clazz("normal")
        .renderer("region")
        .updated(System.currentTimeMillis())
        .build());

    //For each dataCenter we determine the nodes and connections
    dataCenters.forEach(dataCenter -> {
      nodes.add(Node.builder()
          .name(dataCenter)
          .clazz("normal")
          .renderer("region")
          .nodes(createNodesInDataCenter(dataCenter))
          .connections(createConnectionsInDataCenter(dataCenter))
          .updated(System.currentTimeMillis())
          .build()
      );
    });
    return nodes;
  }

  /**
   * Given the region nodes [DCR,WPR] we determine the connections between the
   * regions by summing the metrics of the data center connections with the
   * 'INTERNET' node.
   */
  private List<Connection> getRegionConnections(List<Node> regionNodes) {
    List<Connection> regionConnections = new ArrayList<>();
    for (Node regionNode : regionNodes) {
      if (regionNode.getConnections() != null) {
        // Initialize the metrics
        Metrics metrics = Metrics.builder()
            .normal(0d)
            .danger(0d)
            .build();

        // Filter the data center connections with the INTERNET node
        // and sum the metrics
        regionNode.getConnections().stream()
            .filter(connection -> connection.getSource().equalsIgnoreCase(INTERNET))
            .map(connection -> connection.getMetrics())
            .forEach(metrics::add);

        //Create a region connection with the summed metrics
        regionConnections.add(Connection.builder()
            .source(INTERNET)
            .target(regionNode.getName())
            .metrics(metrics)
            .build());
      }
    }
    return regionConnections;
  }

  private List<Node> createNodesInDataCenter(String dataCenter) {
    Set<String> applications = discoveryClient.getApplications(dataCenter);

    List<Node> nodes = new ArrayList<>();

    //We always add the INTERNET node
    nodes.add(Node.builder()
        .name(INTERNET)
        .clazz("normal")
        .renderer("focusedChild")
        .build());

    if (properties.getExtraNodes() != null) {
      properties.getExtraNodes().forEach(extraNode -> {
        nodes.add(Node.builder()
            .name(extraNode)
            .clazz("normal")
            .renderer("focusedChild")
            .build());
      });
    }

    applications.stream()
        .map(application -> Node.builder()
            .displayName(application.toLowerCase())
            .name(application.toLowerCase())
            .clazz("normal")
            .renderer("focusedChild")
            .updated(System.currentTimeMillis())
            .build()
        ).forEach(nodes::add);
    return nodes;
  }

  private List<Connection> createConnectionsInDataCenter(String dataCenter) {
    List<Instance> instances = discoveryClient.getInstances(dataCenter);

    List<Connection> connections = new ArrayList<>();

    for (Instance instance : instances) {
      String service = instance.getApp().toLowerCase();

      if (!isIncludedService(service)) {
        continue;
      }

      if (isEdgeService(service)) {
        //For edge-services we determine the incoming request as being the metrics from internet to 'edge-service'
        Metrics metrics = requestMetricsClient.getIncomingRequestMetrics(instance);

        //Find an optional existing connection from a previous instance
        addOrUpdateConnection(connections, INTERNET, service, metrics);
      }

      //For all nodes we determine the outgoing request metrics
      requestMetricsClient.getOutgoingRequestMetrics(instance).forEach(connection -> {
        addOrUpdateConnection(connections, connection.getSource(), connection.getTarget(), connection.getMetrics());
      });
    }
    return connections;
  }

  private void addOrUpdateConnection(List<Connection> connections, String source, String target, Metrics metrics) {
    Optional<Connection> optionalConnection = connections.stream().filter(c -> c.getSource().endsWith(source) && c.getTarget().equals(target)).findFirst();
    if (optionalConnection.isPresent()) {
      optionalConnection.get().getMetrics().add(metrics);
    } else {
      connections.add(
          Connection.builder()
              .source(source)
              .target(target)
              .metrics(metrics)
              .build()
      );
    }
  }

  private boolean isEdgeService(String service) {
    return properties.getEdgeServices().contains(service);
  }

  private boolean isIncludedService(String service) {
    return properties.getIncludedServices() != null && properties.getIncludedServices().contains(service);
  }

}
