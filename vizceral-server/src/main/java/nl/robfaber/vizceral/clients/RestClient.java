package nl.robfaber.vizceral.clients;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static nl.rofaber.metrics.RequestMetricsService.FROM_SERVICE_HEADER;
import static nl.rofaber.metrics.RequestMetricsService.TO_SERVICE_HEADER;

@Service
@RequiredArgsConstructor
@Slf4j
public class RestClient {

  private final RestTemplate restTemplate;

  @Value("${spring.application.name}")
  private String fromService;


  public <T> T executeRequest(String toService, String url, Class<T> responseType) {

    log.debug("Calling url {} of service {}", url, toService.toLowerCase());

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add(TO_SERVICE_HEADER, toService.toLowerCase());
    headers.add(FROM_SERVICE_HEADER, fromService.toLowerCase());

    HttpEntity<?> entity = new HttpEntity<>(headers);

    ResponseEntity<T> responseEntity;
    try {
      responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, responseType);
    } catch (HttpClientErrorException e) {
      log.debug("Service {} does not support {}", toService.toLowerCase(), url);
      return null;
    } catch (RuntimeException e) {
      log.warn("Service {} failed request {}", toService.toLowerCase(), url);
      throw e;
    }
    return responseEntity.getBody();
  }
}
