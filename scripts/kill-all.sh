ports='8090 8761 8888 8762 8763 8880 8881 8091 8092 9001 9002 9003 9004 9010 9011 9012 9013 8870 8871 8886'


for port in $ports; do
  printf "killing process holding port: ${port}"
  process=$(lsof -i :$port -sTCP:LISTEN | grep 'java\|node\|grunt' | tail -1 | awk '{print $2}')
  if [ ! -z "$process" ]; then
    kill $process &
    printf " - killed (${process})"
  fi
  printf "\n"
done

#clean_pid_files
#
#verify_pid_files_are_gone
#
#verify_ports_are_open 10


rm *.log
