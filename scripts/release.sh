#!/usr/bin/env bash

crt_branch=`git rev-parse --abbrev-ref HEAD`

# Bump snapshot version to release version
version="$(grep '<project' pom.xml -A5 | grep '<version>' | egrep -o '[0-9]+.[0-9]+.[0-9]+')"
echo "Found version $version"

branch="release-$version"
git fetch && git checkout origin/master && git checkout -b $branch

mvn versions:set versions:commit -DnewVersion=${version} --batch-mode

# Commit, tag, and push release changes to git
git add '**/pom.xml'
git add './pom.xml'
git commit -m "Set version to $version"

artifact=`echo $'setns x=http://maven.apache.org/POM/4.0.0\ncat /x:project/x:artifactId/text()' | xmllint --shell pom.xml | grep -v /`
tag="$artifact-$version"
git tag -a $tag -m $tag >/dev/null

mr_url=`git push --set-upstream origin $branch 2>&1 | grep merge_requests | grep -o 'http.*'`
git push origin $tag

open -a "/Applications/Google Chrome.app" $mr_url
if [[ $? != 0 ]]; then
  echo "Could not open MR URL. Please go to: $mr_url"
fi

git checkout $crt_branch
git branch -d $branch
