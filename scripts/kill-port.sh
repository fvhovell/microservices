
printf "killing process holding port: $1"
process=$(lsof -i :$1 -sTCP:LISTEN | grep 'java\|node\|grunt' | tail -1 | awk '{print $2}')
if [ ! -z "$process" ]; then
  kill $process &
  printf " - killed (${process})"
fi
printf "\n"
